<%@ include file="includes/cabecera.jsp" %>
<body class="container">
<p><h1>�LTIMAS NOTICIAS</h1></p>
<section>

<c:forEach items="${noticias.values()}" var="noticia">
	<article>
		<h2>${noticia.titulo}</h2>
		<p>
		<time datetime="${noticia.fecha}">
			<fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss"
							value="${noticia.fecha}" />
		</time>
			 escrito por <cite>${noticia.autor}</cite>
		</p>
		<p>
			<img src="media/${noticia.id}.jpg" alt=""></img>
		</p>
		<p>
			${noticia.descripcion}
		</p>
		<!-- <p>
			<a href="#">leer mas..</a>
		</p> -->
	</article>
</c:forEach>
</section>
<p><a href="login.jsp">Acceder a mantenimiento <img class="mini" src="media/mant.png" alt=""></a></p>
<%@ include file="includes/pie.jsp" %>