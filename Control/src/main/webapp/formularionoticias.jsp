<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/estilos.css"
	media="screen" />
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Insert title here</title>
</head>
<body class="container">
	<form action="repartir" method="post" class="form-group">
		<fieldset>
			<legend></legend>
			<p class="row">
				<label class="col-sm-2 col-form-label"  for="titulo">Titulo</label> <input ${bloqueo} class="form-control col-sm-10" id="titulo" name="titulo" type="text" value="${noticia.titulo}">
			</p>
			<p class="row">
				<label class="col-sm-2 col-form-label" for="descripcion">Descripcion</label> <input ${bloqueo} class="form-control col-sm-10" id="descripcion" name="descripcion" value="${noticia.descripcion}" type="text">
			</p>
			<p class="row">
				<label class="col-sm-2 col-form-label" for="autor">Autor</label> <input ${bloqueo} class="form-control col-sm-10" id="autor" name="autor" value="${noticia.autor}"type="text">
			</p>
			<input type="hidden" id="accion" name="accion" value="${accion}">
			<input type="hidden" id="id" name="id" value="${noticia.id}">
			<p><span ><%=request.getAttribute("error") != null ? request.getAttribute("error") : ""%></span></p>
			<button class="btn btn-primary">${accion}</button>
		</fieldset>
	</form>
<%@ include file="includes/pie.jsp" %>