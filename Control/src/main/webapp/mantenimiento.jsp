<%@ include file="includes/cabecera.jsp" %>
<body class="container">
	<table class="table table-hover vertical">
		<tr class="cabecera">
			<th scope="col">ID</th>
			<th scope="col">Imagen</th>
			<th scope="col">Fecha</th>
			<th scope="col">Titulo</th>
			<th scope="col">Dscripcion</th>
			<th scope="col">Autor</th>
			<th scope="col"></th>
			<th scope="col"></th>
		</tr>
		<c:forEach items="${noticias.values()}" var="noticia">
			<tr>
				<th class="align-middle" scope="row">${noticia.id}</th>
				<td class="align-middle"><img class="maspeque" src="media/${noticia.id}.jpg" alt=""></td>
				<td class="align-middle">
				<time datetime="${noticia.fecha}">
					<fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${noticia.fecha}" />
				</time>
				</td>
				<td class="align-middle">${noticia.titulo}</td>
				<td class="align-middle">${noticia.descripcion}</td>
				<td class="align-middle">${noticia.autor}</td>
				<td class="align-middle"><a href="repartir?accion=Modificar&id=${noticia.id}"><img
						class="mini" src="media/pen.png" /></a></td>
				<td class="align-middle"><a href="repartir?accion=Borrar&id=${noticia.id}"><img
						class="mini" src="media/trash.png" /></a></td>
			</tr>
		</c:forEach>
		<tr class="nada">
			<td colspan=7 scope="row">
				<a  href="repartir?accion=Guardar">Agregar noticia <img	class="mini" src="media/add.png" /></a>
			</td>
			<td>
			<a class="derecha" href="principal.jsp">Volver <img class="mini"src="media/exit.png" /></a>
			</td>
		</tr>
	</table>
<%@ include file="includes/pie.jsp" %>