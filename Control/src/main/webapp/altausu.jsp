<%@ include file="includes/cabecera.jsp" %>
<body class="container">
	<fieldset>
		<legend>Nuevo usuario</legend>
		<form action="altausuario" method="post" class="form-group">
			<p class="row">
				<label class="col-sm-2 col-form-label" for="usuario">Usuario</label> <input class="form-control col-sm-10" id="usuario"
					name="usuario" type="text" required>
			</p>
			<p class="row">
				<label class="col-sm-2 col-form-label" for="usuario">Contraseņa</label> <input class="form-control col-sm-10" id="claveacceso"
					name="claveacceso" type="password" required>
			</p>
			<p class="row">
				<button class="bt btn-primary">Registrarte</button>
			</p>
		</form>
		<p>
			<%=request.getAttribute("error") != null ? request.getAttribute("error") : ""%>
		</p>
		<form action="login.jsp" class="form-group">
			<p class="row">
				<button class="bt btn-primary">Volver</button>
			</p>
		</form>


	</fieldset>
<%@ include file="includes/pie.jsp" %>