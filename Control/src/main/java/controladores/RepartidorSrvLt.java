package controladores;

import java.io.IOException;
import java.util.Date;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.Noticia;

@WebServlet("/repartir")
public class RepartidorSrvLt extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public RepartidorSrvLt() {
        super();
        
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String accion = request.getParameter("accion");
		

		
		@SuppressWarnings("unchecked")
		TreeMap<Long, Noticia> noticias= (TreeMap<Long, Noticia>) request.getServletContext().getAttribute("noticias");
		
		if(accion==null) {
			response.sendRedirect("mantenimiento.jsp");
			
		}else {
			
			if("Modificar".equals(accion)){
				Long id = Long.parseLong(request.getParameter("id"));
				Noticia noticia = noticias.get(id);
				request.setAttribute("noticia", noticia);
				request.setAttribute("accion", accion);
				request.getRequestDispatcher("formularionoticias.jsp").forward(request, response);
				
				
			}else if( "Borrar".equals(accion)) {
				Long id = Long.parseLong(request.getParameter("id"));
				Noticia noticia = noticias.get(id);
				request.setAttribute("noticia", noticia);
				request.setAttribute("accion", accion);
				request.setAttribute("bloqueo", "readonly");
				request.getRequestDispatcher("formularionoticias.jsp").forward(request, response);
			
			}else if("Guardar".equals(accion)) {
				request.setAttribute("accion", accion);	
				request.getRequestDispatcher("formularionoticias.jsp").forward(request, response);
			}
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String accion = request.getParameter("accion");
		String titulo = request.getParameter("titulo");
		String autor = request.getParameter("autor");
		String descripcion = request.getParameter("descripcion");
		Date fecha = new Date();
		Long id = null;
		@SuppressWarnings("unchecked")
		TreeMap<Long,Noticia> noticias=(TreeMap<Long,Noticia>)request.getServletContext().getAttribute("noticias");
		
		if("Guardar".equals(accion) || "Modificar".equals(accion)) {
			if("Guardar".equals(accion)) {
				if (noticias==null) {
					id=1L;
					noticias = new TreeMap<Long,Noticia>();
				}else {
					if(!noticias.isEmpty()) {
						id=noticias.lastKey()+1L;
					}else {
						id=1L;
					}					
				}
			}else {
				id =Long.parseLong(request.getParameter("id"));
			}
			Noticia noticia= new Noticia(id,titulo,descripcion,fecha,autor);
			if (noticia.isCorrecto()) {
				noticias.put(id, noticia);
				request.getServletContext().setAttribute("noticias", noticias);
				request.getRequestDispatcher("mantenimiento.jsp").forward(request, response);
			}else {
				if(noticia.getErrorAutor()!=null && noticia.getErrorAutor().trim().length()!=0) {
					request.setAttribute("error", "Error en autor");
				}else if(noticia.getErrorId()!=null && noticia.getErrorId().trim().length()!=0) {
					request.setAttribute("error", "Error en id");
				}else if(noticia.getErrordescripcion()!=null && noticia.getErrordescripcion().trim().length()!=0) {
					request.setAttribute("error", "Error en descripcion");
				}else if(noticia.getErrorTitulo()!=null && noticia.getErrorTitulo().trim().length()!=0) {
					request.setAttribute("error", "Error en Titulo");
				}else if(noticia.getErrorFecha()!=null && noticia.getErrorFecha().trim().length()!=0) {
					request.setAttribute("error", "Error en fecha");
				}
				request.setAttribute("accion", accion);
				request.getRequestDispatcher("formularionoticias.jsp").forward(request, response);
			}
			
		}else if("Borrar".equals(accion)) {
			id =Long.parseLong(request.getParameter("id"));
			noticias.remove(id);
			request.getServletContext().setAttribute("noticias", noticias);
			request.getRequestDispatcher("mantenimiento.jsp").forward(request, response);
		}
	}

}
