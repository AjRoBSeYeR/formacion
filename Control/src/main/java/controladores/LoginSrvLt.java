package controladores;

import java.io.IOException;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.UsuarioLogin;

@WebServlet("/login")
public class LoginSrvLt extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginSrvLt() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("login.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String usu = request.getParameter("usuario");
		String pass = request.getParameter("claveacceso");
		@SuppressWarnings("unchecked")
		TreeMap<Long, UsuarioLogin> usuarios = (TreeMap<Long, UsuarioLogin>) request.getServletContext()
				.getAttribute("usuarios");
		if (usuarios == null) {
			request.setAttribute("error", "No hay usuarios regisrados, por favor date de alta");
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		} else {
			for (UsuarioLogin usuario : usuarios.values()) {
				if (usuario.getNombreusuario().equals(usu) && usuario.getClaveacceso().equals(pass)) {
					request.getRequestDispatcher("mantenimiento.jsp").forward(request, response);
				} else {
					request.setAttribute("error", "Datos de login incorrectos");
					request.getRequestDispatcher("login.jsp").forward(request, response);
				}
			}
		}

	}

}
