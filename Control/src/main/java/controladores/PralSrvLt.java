package controladores;

import java.io.IOException;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.Noticia;

@WebServlet("/principal")
public class PralSrvLt extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PralSrvLt() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		TreeMap<Long,Noticia> noticias =(TreeMap<Long, Noticia>)request.getServletContext().getAttribute("noticias");
		request.getServletContext().setAttribute("noticias",noticias);
		request.getRequestDispatcher("principal.jsp").forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
