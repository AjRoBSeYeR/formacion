package controladores;

import java.io.IOException;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelos.UsuarioLogin;

@WebServlet("/altausuario")
public class AltaUsuarioSrvLt extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public AltaUsuarioSrvLt() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String usu = request.getParameter("usuario");
		String pass = request.getParameter("claveacceso");
		String error;
		Long id;
		UsuarioLogin usuario = new UsuarioLogin(usu, pass);
		@SuppressWarnings("unchecked")
		TreeMap<Long,UsuarioLogin> usuarios=(TreeMap<Long,UsuarioLogin>)request.getServletContext().getAttribute("usuarios");
		if(usuarios==null) {
			usuarios= new TreeMap<Long,UsuarioLogin>();
			id=1L;
		}else {
			id=usuarios.lastKey()+1L;
		}
		if (usuario.isCorrecto()) {			
			usuarios.put(id, usuario);	
			request.getServletContext().setAttribute("usuarios",usuarios);
			request.getRequestDispatcher("login.jsp").forward(request, response);
		} else {
			if (!"".equals(usuario.getErrorUsuario()) || usuario.getErrorUsuario()!=null) {
				error= usuario.getErrorUsuario();
			} else {
				error= usuario.getErrorPass();
			}
			request.setAttribute("error", error);
			request.getRequestDispatcher("altausu.jsp").forward(request, response);
		}

		//doGet(request, response);
	}

}
