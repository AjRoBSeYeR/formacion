package modelos;

public class UsuarioLogin {
	private Long id;
	private String nombreUsuario;
	private String claveAcceso;
	private boolean hayErrores = false;
	private String nombrecompleto;
	
	private String errorUsuario=null;
	private String errorPass=null;
	
	public String getErrorUsuario() {
		return errorUsuario;
	}
	public void setErrorUsuario(String errorusuario) {
		hayErrores=true;
		this.errorUsuario = errorusuario;
	}
	public String getErrorPass() {
		return errorPass;
	}
	public void setErrorPass(String errorpass) {
		hayErrores=true;
		this.errorPass = errorpass;
	}
	public UsuarioLogin(String nombreusuario, String claveacceso) {
		super();
		setNombreusuario(nombreusuario);
		setClaveacceso(claveacceso);
	}
	public UsuarioLogin() {
		
	}
	@Override
	public String toString() {
		return "UsuarioLogin [nombreusuario=" + nombreUsuario + ", claveacceso=" + claveAcceso + "]";
	}
	public String getNombreusuario() {
		return nombreUsuario;
	}
	public void setNombreusuario(String nombreusuario) {
		if(nombreusuario==null || nombreusuario.trim().length()==0) {
			setErrorUsuario("Error en el usuario");
		}
		this.nombreUsuario = nombreusuario;
	}
	public String getClaveacceso() {
		return claveAcceso;
	}
	public void setClaveacceso(String claveacceso) {
		if(claveacceso==null || claveacceso.trim().length()==0) {
			setErrorUsuario("Error en la clave");
		}else if (claveacceso.length()<8) {
			setErrorUsuario("clave demasiado corta");
		}else if (!claveacceso.matches("^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{8,16}$")) {
			setErrorUsuario("la clave debe contener al menos una may�scula, una min�cula y un d�gito");
		}
		
		this.claveAcceso = claveacceso;
	}
	public boolean isCorrecto() {
		return !hayErrores;
	}

}
