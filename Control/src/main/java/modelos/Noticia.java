package modelos;

import java.util.Date;

public class Noticia {
	private Long id;
	private String titulo;
	private String descripcion;
	private Date fecha;
	private String autor;
	
	private String errorId=null;
	private String errorTitulo=null;
	private String errordescripcion=null;
	private String errorFecha=null;
	private String errorAutor=null;
	
	private boolean hayError=false;
	
	public String getErrorId() {
		return errorId;
	}
	public void setErrorId(String errorId) {
		hayError=true;
		this.errorId = errorId;
	}
	public String getErrorTitulo() {
		return errorTitulo;
	}
	public void setErrorTitulo(String errorTitulo) {
		hayError=true;
		this.errorTitulo = errorTitulo;
	}
	public String getErrordescripcion() {
		return errordescripcion;
	}
	public void setErrordescripcion(String errordescripcion) {
		hayError=true;
		this.errordescripcion = errordescripcion;
	}
	public String getErrorFecha() {
		return errorFecha;
	}
	public void setErrorFecha(String errorFecha) {
		hayError=true;
		this.errorFecha = errorFecha;
	}
	public String getErrorAutor() {
		return errorAutor;
	}
	public void setErrorAutor(String errorAutor) {
		hayError=true;
		this.errorAutor = errorAutor;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		if(id==null) {
			setErrorId("el id està vacìo");
		}else if (id.toString().trim().length()==0) {
			setErrorId("el id està vacìo");
		}else if (id<=0L) {
			setErrorId("id invàlido");
		}
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		if(titulo==null) {
			setErrorTitulo("el titulo està vacìo");
		}else if (titulo.trim().length()==0) {
			setErrorTitulo("el titulo està vacìo");
		}
		this.titulo = titulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		if(descripcion==null) {
			setErrordescripcion("la descripcion està vacìa");
		}else if (descripcion.trim().length()==0) {
			setErrordescripcion("la descripcion està vacìa");
		}
		this.descripcion = descripcion;
	}
	public Date getFecha() {

		return fecha;
	}
	public void setFecha(Date fecha) {
		if(fecha==null) {
			setErrorFecha("la fecha està vacìo");
		}else if (fecha.toString().trim().length()==0) {
			setErrorFecha("la fecha està vacìo");
		}
		this.fecha = fecha;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		if(autor==null) {
			setErrorAutor("el autor està vacìo");
		}else if (autor.trim().length()==0) {
			setErrorAutor("el autor està vacìo");
		}
		this.autor = autor;
	}
	public boolean isCorrecto() {
		return !hayError;
	}
	
	@Override
	public String toString() {
		return "Noticia [id=" + id + ", titulo=" + titulo + ", descripcion=" + descripcion + ", fecha=" + fecha
				+ ", autor=" + autor + "]";
	}
	public Noticia(Long id, String titulo, String descripcion, Date fecha, String autor) {
		super();
		setId(id);
		setTitulo(titulo);
		setDescripcion(descripcion);
		setFecha(fecha);
		setAutor(autor);
	}
	public Noticia() {
		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autor == null) ? 0 : autor.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((errorAutor == null) ? 0 : errorAutor.hashCode());
		result = prime * result + ((errorFecha == null) ? 0 : errorFecha.hashCode());
		result = prime * result + ((errorId == null) ? 0 : errorId.hashCode());
		result = prime * result + ((errorTitulo == null) ? 0 : errorTitulo.hashCode());
		result = prime * result + ((errordescripcion == null) ? 0 : errordescripcion.hashCode());
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + (hayError ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Noticia other = (Noticia) obj;
		if (autor == null) {
			if (other.autor != null)
				return false;
		} else if (!autor.equals(other.autor))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (errorAutor == null) {
			if (other.errorAutor != null)
				return false;
		} else if (!errorAutor.equals(other.errorAutor))
			return false;
		if (errorFecha == null) {
			if (other.errorFecha != null)
				return false;
		} else if (!errorFecha.equals(other.errorFecha))
			return false;
		if (errorId == null) {
			if (other.errorId != null)
				return false;
		} else if (!errorId.equals(other.errorId))
			return false;
		if (errorTitulo == null) {
			if (other.errorTitulo != null)
				return false;
		} else if (!errorTitulo.equals(other.errorTitulo))
			return false;
		if (errordescripcion == null) {
			if (other.errordescripcion != null)
				return false;
		} else if (!errordescripcion.equals(other.errordescripcion))
			return false;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (hayError != other.hayError)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		return true;
	}
	
	

}
